# git-pretty-alias

## _Tools for editing complex git aliases_

`git-pretty-alias` is a set of scripts (actually, git aliases themselves) to convert between `.gitconfig` shell-alias format (with escaped newlines and quotes) and "normal" (indented, unescaped) shell code. This makes writing complex shell aliases feasible. There are also commands for wrapping / unwrapping bash code in git's `sh`-based aliases (which requires an extra level of quote-escaping).

## Copyright

Copyright 2021 `Alin Mr. <almr.oss@outlook.com>`. MIT license.

## Usage

In the cloned repo, the following will self-edit the main alias; delete everything before saving to avoid overwriting (or use `git reset --hard`).
```
GIT_CONFIG=gitconfig git pprint-alias-edit pprint-alias-edit
```

You can similarly point `GIT_CONFIG` to any file; if undefined, the scripts will write to (but not read from) `/tmp/gitconfig.tmp` by default.

_Note_: a "bad" line in any of the relevant `gitconfig's` (there are 4 of them: system, global / per-user, local / per-repo, and possibly per-worktree) can prevent git from running at all. Rename the offending file, `cd` to a different location etc.

## Installation

Simply add to your `~/.gitconfig` the path to the provided aliases; assuming you are in the cloned repo:
```
c=$PWD/gitconfig; test -r "$c" &&
  git config --global --add include.path "$c"
```
